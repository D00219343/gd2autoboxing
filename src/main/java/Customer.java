

package main.java;
import java.util.ArrayList;

public class Customer
{
    private String name;
    private ArrayList<Double> transactions = new ArrayList<Double>();

    //Constructor
    public Customer(String name, ArrayList<Double> transactions)
    {
        this.name = name;
        this.transactions = transactions;
    }

    //Getters
    public String getName() { return name; }
    public ArrayList<Double> getTransactions() { return transactions; }


    //Setters
    public void setName(String name) { this.name = name; }
    public void setTransactions(ArrayList<Double> transactions) { this.transactions = transactions; }

    public void addTransaction(double amount)
    {
        this.transactions.add(amount);
    }

    public Customer(String name, double initialAmount)
    {
        this.name = name;
        this.transactions = new ArrayList<Double>();
        addTransaction(initialAmount);
    }
}
