package main.java;

import java.util.ArrayList;

class Branch
{
    private ArrayList<Customer> customers = new ArrayList<Customer>();

    private String name;

    public Branch(String branchName, ArrayList<Customer> customers)
    {
        this.name = branchName;
        this.customers = customers;
    }

    public Branch(String branchName)
    {
        this.name = branchName;
    }

    public void addCustomer(String name, double initialAmount)
    {
        customers.add(new Customer(name, initialAmount));

    }

}


public class Bank
{
    private static String name;
    private static ArrayList<Branch> branches = new ArrayList<Branch>();

    public Bank(String name)
    {
        this.name = name;
    }

//    public static void addBranch(String branchName){
//        branches.add(new Branch(branchName));
//    }

    public static void addBranch(String branchName, ArrayList<Customer> customers){
        branches.add(new Branch(branchName, customers));
    }

    public void addCustomerToBranch(String name, double initialAmount, String branch)
    {
        int ans = branches.indexOf("branch");
        branches.get(ans).addCustomer(name, initialAmount);
    }

    public String getBankName()
    {
        return name;
    }

    public void setBankName(String Name)
    {
        this.name = Name;
    }

//    public static void main(String[] args) {
//        addBranch("Dundalk");
//        addBranch("Drogheda");
//        addBranch("Blanch");
//        addBranch("Dublin");
//    }




}
